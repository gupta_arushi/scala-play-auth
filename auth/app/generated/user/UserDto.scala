package com.rapidfork.auth.generated.user

import play.api.libs.json.{Json, OFormat}

case class AddUserDto (
                        firstName:  String,
                        lastName:  String,
                        mobile:  Long,
                        email:  Option[String] ,
                        password:  String,
                      )

object AddUserDto {
  implicit val format: OFormat[AddUserDto] = Json.format[AddUserDto]
}

object UserDto {

}

