package com.rapidfork.auth.generated.emailconfirmedlog

import play.api.libs.json.{Json, OFormat}

case class AddEmailConfirmedLogDto (
                        userId:  Long,
                        email:  String,
                        token:  String,
                        isExpired:  Boolean,
                        isActive:  Boolean,
                      )

object AddEmailConfirmedLogDto {
  implicit val format: OFormat[AddEmailConfirmedLogDto] = Json.format[AddEmailConfirmedLogDto]
}

object EmailConfirmedLogDto {

}

