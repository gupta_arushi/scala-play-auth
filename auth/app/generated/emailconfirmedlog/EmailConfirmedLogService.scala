package com.rapidfork.auth.generated.emailconfirmedlog

import javax.inject._
import scala.concurrent.Future

@Singleton
class EmailConfirmedLogService @Inject() (protected val emailconfirmedlogRepository: EmailConfirmedLogRepository) {

  def addEmailConfirmedLog(emailconfirmedlog: AddEmailConfirmedLogDto): Future[String] = {
    emailconfirmedlogRepository.add(emailconfirmedlog)
  }

  def deleteEmailConfirmedLog(id: Long): Future[Int] = {
    emailconfirmedlogRepository.delete(id)
  }

  def getEmailConfirmedLog(id: Long): Future[Option[EmailConfirmedLogSchema.EmailConfirmedLog]] = {
    emailconfirmedlogRepository.get(id)
  }

  def listAllEmailConfirmedLogs: Future[Seq[EmailConfirmedLogSchema.EmailConfirmedLog]] = {
    emailconfirmedlogRepository.listAll
  }
}

