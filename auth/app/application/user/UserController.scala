package com.rapidfork.auth.application.user

import javax.inject._
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc.{Action, AnyContent, ControllerComponents, Request}

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration.Duration
import com.rapidfork.auth.generated.user
import com.rapidfork.auth.utils.{AuthAction, UserRequest}

@Singleton
class UserController @Inject()(controllerComponents: ControllerComponents, userService: UserService, authAction: AuthAction)(override implicit val ec: ExecutionContext) extends user.UserController(controllerComponents, userService) {

  def example: Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    userService.listAllUsers.map(
      users => Ok(Json.toJson(users.map(_.toPublic)))
    )
  }

  def register: Action[AnyContent] = Action.async {
    implicit request: Request[AnyContent] =>
      request.body.asJson match {
        case Some(json) =>
          json.validate[user.AddUserDto] match {
            case JsSuccess(requestBody, _) =>
              userService.registerUser(requestBody)
              Future {Ok}
            case e: JsError => Future {
              BadRequest("Missing Payload")
            }
          }
        case None => Future {
          BadRequest("Missing Payload")
        }
      }
  }

  def login: Action[AnyContent] = Action.async {
    implicit request: Request[AnyContent] =>
      request.body.asJson match {
        case Some(json) =>
          json.validate[UserLoginDto] match {
            case JsSuccess(requestBody, _) =>
              userService.validateUser(requestBody)
            case e: JsError => Future {
              BadRequest("Missing Payload")
            }
          }
        case None => Future {
          BadRequest("Missing Payload")
        }
      }
  }

  def getProfile: Action[AnyContent] = authAction.async {
    implicit request =>
      Future { Ok(Json.toJson(request.user.get)) }
  }
}

