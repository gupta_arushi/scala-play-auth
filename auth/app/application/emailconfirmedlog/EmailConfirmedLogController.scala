package com.rapidfork.auth.application.emailconfirmedlog

import javax.inject._
import play.api.libs.json.Json
import play.api.mvc.{Action, AnyContent, ControllerComponents, Request}
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration.Duration

import com.rapidfork.auth.generated.emailconfirmedlog

@Singleton
class EmailConfirmedLogController @Inject()(controllerComponents: ControllerComponents, emailconfirmedlogService: EmailConfirmedLogService)(override implicit val ec: ExecutionContext) extends emailconfirmedlog.EmailConfirmedLogController(controllerComponents, emailconfirmedlogService) {

  def example: Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    emailconfirmedlogService.listAllEmailConfirmedLogs.map(
      emailconfirmedlogs => Ok(Json.toJson(emailconfirmedlogs.map(_.toPublic)))
    )
  }
}

