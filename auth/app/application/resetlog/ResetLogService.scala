package com.rapidfork.auth.application.resetlog

import javax.inject._

import scala.concurrent.{ExecutionContext, Future}
import com.rapidfork.auth.generated.resetlog
import com.rapidfork.auth.generated.resetlog.ResetLogSchema

@Singleton
class ResetLogService @Inject()(resetlogRepository: ResetLogRepository)(implicit ec: ExecutionContext) extends resetlog.ResetLogService(resetlogRepository) {
  def getResetLogByEmail(email: String): Future[Seq[resetlog.ResetLogPublic]] = resetlogRepository.getByEmail(email).map(rl => rl.map(_.toPublic))

  def expireOldTokens(email: String): Future[Int] = resetlogRepository.updateMultiExpire(email)

  def validateResetToken(email: String, token: String): Future[Option[ResetLogSchema.ResetLog]] = resetlogRepository.getByTokenAndEmail(email, token)

  def updatePasswordSuccess(email: String, token: String): Future[Int] = resetlogRepository.updatePassword(email, token)
}


