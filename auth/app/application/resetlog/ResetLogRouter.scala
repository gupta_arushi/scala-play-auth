package com.rapidfork.auth.application.resetlog

import javax.inject._
import play.api.routing.Router.Routes
import play.api.routing.sird._

import com.rapidfork.auth.generated.resetlog

class ResetLogRouter @Inject()(controller: ResetLogController ) extends resetlog.ResetLogRouter(controller) {
  override def routes: Routes = super.routes orElse  {
    case GET(p"/example") => controller.example
    case POST(p"/forgot") => controller.forgot
    case POST(p"/validate") => controller.validateToken
    case POST(p"/reset") => controller.changePassword
  }
}


