package com.rapidfork.auth.utils

import java.time.Clock
import java.util.{Date, UUID}

import pdi.jwt._
import pdi.jwt.exceptions.JwtValidationException
import play.api.Configuration
import play.api.libs.json.{Json, OFormat}

import scala.util.{Failure, Success, Try}

case class JwtData(
                  userId: Long,
                  issuer: String,
                  time: Long,
                )
object JwtData {
  implicit val format: OFormat[JwtData] = Json.format[JwtData]
}

object JwtUtil {
  implicit val clock: Clock = Clock.systemUTC

  def generate(issuer: String = "RapidFork", userId: Long, expireIn: Long)(implicit configuration: Configuration): String = {
    val key = configuration.get[String]("play.http.secret.key")
    val jtwClaim = JwtClaim(Json.stringify(Json.obj(("userId", userId), ("issuer", issuer), ("time", new Date().getTime))))
//    JwtSession() + ("userId", userId) + ("issuer", issuer) + ("time", new Date().getTime)
    Jwt.encode(jtwClaim.issuedNow.expiresIn(expireIn), key, JwtAlgorithm.HS256)
  }


  def renew(session:JwtSession): String = {
    session.refresh().serialize
  }

  def deSerialize(token: String)(implicit configuration: Configuration): JwtData = {
    val jwtData = JwtSession.deserialize(token)

    val data = JwtData(
                        userId = jwtData.getAs[Long]("userId").get,
                        issuer = jwtData.getAs[String]("issuer").get,
                        time = jwtData.getAs[Long]("time").get
                      )
    data
  }

  def validate(token: String)(implicit configuration: Configuration): Try[Boolean] = {
    try {
      val key = configuration.get[String]("play.http.secret.key")
      val algo = Seq(JwtAlgorithm.HS256)

      Jwt.validate(token, key, algo)
      Success(Jwt.isValid(token, key, algo))
    } catch {
      case e: Throwable => Failure(new Exception("Auth Token is Invalid or Expired"))
    }

  }
}