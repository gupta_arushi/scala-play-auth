package com.rapidfork.auth.utils

import play.api.Configuration
import javax.inject.Inject

import com.sendgrid
import java.io.IOException
class SendgridEmail @Inject()(configuration: Configuration) {
  def sendMail(fromEmail: String, subjectString: String, toEmail: String, contentString: String, html: String): Unit = {
    // todo : User env variable
    val from = new sendgrid.Email(fromEmail)
    val subject = subjectString
    val to = new sendgrid.Email(toEmail)
    val contentPlain = new sendgrid.Content("text/plain", contentString)
    val contentHtml = new sendgrid.Content("text/html", html)

    val mail = new sendgrid.Mail(from, subject, to, contentPlain)
    mail.addContent(contentHtml)


    val sg = new sendgrid.SendGrid(configuration.get[String]("sendgrid.apiKey"))
    val request = new sendgrid.Request();
    try {
      request.setMethod(sendgrid.Method.POST)
      request.setEndpoint("mail/send")
      request.setBody(mail.build())
      val response = sg.api(request)
    } catch {
      case ex: IOException =>
        throw ex
    }
  }

  def sendMail(fromEmail: String, subjectString: String, toEmail: String, contentString: String): Unit = {
    // todo : User env variable
    val from = new sendgrid.Email(fromEmail)
    val subject = subjectString
    val to = new sendgrid.Email(toEmail)
    val contentPlain = new sendgrid.Content("text/plain", contentString)

    val mail = new sendgrid.Mail(from, subject, to, contentPlain)
    val sg = new sendgrid.SendGrid(configuration.get[String]("sendgrid.apiKey"))
    val request = new sendgrid.Request();
    try {
      request.setMethod(sendgrid.Method.POST)
      request.setEndpoint("mail/send")
      request.setBody(mail.build())
      val response = sg.api(request)
    } catch {
      case ex: IOException =>
        throw ex
    }
  }
}


