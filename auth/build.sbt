name := "auth"
organization := "com.rapidfork"

version := "1.0"

scalaVersion := "2.13.3"

libraryDependencies ++= Seq(
  guice,
  "com.typesafe.play" %% "play-slick" % "5.0.0",
  "com.typesafe.play" %% "play-slick-evolutions" % "5.0.0",
  "mysql" % "mysql-connector-java" % "8.0.21"
)

// https://mvnrepository.com/artifact/org.mindrot/jbcrypt
libraryDependencies ++= Seq("org.mindrot" % "jbcrypt" % "0.4")

// https://mvnrepository.com/artifact/com.pauldijou/jwt-play
libraryDependencies ++= Seq("com.pauldijou" %% "jwt-play" % "4.3.0")

libraryDependencies ++= Seq("com.sendgrid" % "sendgrid-java" % "4.2.1")