package eager
import javax.inject.{Inject, Singleton}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import slick.dbio.DBIO

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

import com.rapidfork.auth.generated.user._
import com.rapidfork.auth.generated.resetlog._
import com.rapidfork.auth.generated.emailconfirmedlog._

@Singleton
class StartUpService @Inject() (protected val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {
  val dbConfig = dbConfigProvider.get[JdbcProfile]

  val tableSchemas = List(
    UserSchema.userSchemaCreate, 
    ResetLogSchema.resetlogSchemaCreate, 
    EmailConfirmedLogSchema.emailconfirmedlogSchemaCreate
  )
  val setup = DBIO.sequence(
    tableSchemas
  )

  Await.result(dbConfig.db.run(setup).map(t => t), Duration.Inf)
  println("==============")
  println("Server Started")
  println("==============")

}
